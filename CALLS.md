## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for 6connect. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for 6connect.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Six_connect. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getLirs(limit, offset, q, callback)</td>
    <td style="padding:15px">GET Lirs</td>
    <td style="padding:15px">{base_path}/{version}/ipam/lirs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLir(body, callback)</td>
    <td style="padding:15px">Create LIR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/lirs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLir(id, callback)</td>
    <td style="padding:15px">Delete LIR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/lirs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveLir(id, callback)</td>
    <td style="padding:15px">Retrieve LIR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/lirs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchLir(id, body, callback)</td>
    <td style="padding:15px">Update LIR (PATCH)</td>
    <td style="padding:15px">{base_path}/{version}/ipam/lirs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLir(id, body, callback)</td>
    <td style="padding:15px">Update LIR (PUT)</td>
    <td style="padding:15px">{base_path}/{version}/ipam/lirs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">attributesByResource(id, callback)</td>
    <td style="padding:15px">Attributes by Resource</td>
    <td style="padding:15px">{base_path}/{version}/ipam/management/attributes_by_resource/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mergeIpamTopAggregates(body, callback)</td>
    <td style="padding:15px">Merge IPAM Top aggregates</td>
    <td style="padding:15px">{base_path}/{version}/ipam/management/merge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetblocks(id, rir, lirId, address, type, swipped, assigned, isAggregate, lastUpdateTime, swipTime, cidr, endAddress, mask, topAggregate, genericCode, regionId, vlanId, asn, resourceId, resourceQuery, allowSubAssignments, tags, tagsMode, limit, offset, sort, orderBy, assignTime, reservedTime, parent, ruleId, netHandle, customerHandle, orgId, output, outputName, q, attributes, includeRegionList, includingRegionId, resourceType, meta1, meta2, meta3, meta4, meta5, meta6, meta7, meta8, meta9, meta10, callback)</td>
    <td style="padding:15px">Get netblocks</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetblock(body, callback)</td>
    <td style="padding:15px">Create netblock</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">byRegion(recentAssignments, callback)</td>
    <td style="padding:15px">Get netblock region usage</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/by_region?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directAssignMultiple(body, callback)</td>
    <td style="padding:15px">Direct assign by using Resource ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/direct_assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipamGenerateDns(body, callback)</td>
    <td style="padding:15px">Generate DNS Zones and records based on the provided netblock</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/generate_dns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamAttributes(callback)</td>
    <td style="padding:15px">GET IPAM Attributes</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/ipam_attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIpamAttributesGlobal(attribute, limit, offset, q, callback)</td>
    <td style="padding:15px">IPAM Attributes</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/ipam_attributes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">maskReport(resourceId, tags, regions, callback)</td>
    <td style="padding:15px">Mask Report</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/mask_report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMetaValues(metaId, limit, offset, q, callback)</td>
    <td style="padding:15px">GET unique meta values</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/meta_values/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">smartAssignObject(body, callback)</td>
    <td style="padding:15px">Smart assign by using Resource ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/smart_assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSwip1(cidr, lirId, entityHandle, netHandle, callback)</td>
    <td style="padding:15px">Deassign by using Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/swip/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manualSwip1(cidr, body, callback)</td>
    <td style="padding:15px">SWIP by using netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/swip/{pathv1}/manual?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSwip1(cidr, action, body, callback)</td>
    <td style="padding:15px">SWIP by using Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/swip/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSwip(id, lirId, entityHandle, netHandle, callback)</td>
    <td style="padding:15px">Deassign by using Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/swip/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">manualSwip(id, body, callback)</td>
    <td style="padding:15px">SWIP by using netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/swip/{pathv1}/manual?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSwip(id, action, body, callback)</td>
    <td style="padding:15px">SWIP by using Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/swip/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetblock1(cidr, force, callback)</td>
    <td style="padding:15px">Deletes an aggregate rules using Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveNetblock1(cidr, callback)</td>
    <td style="padding:15px">Retrieve Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNetblock1(cidr, body, callback)</td>
    <td style="padding:15px">Update netblock by using Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNetblock1(cidr, body, callback)</td>
    <td style="padding:15px">Update netblock by using Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetblockAssignments1(cidr, offset, limit, callback)</td>
    <td style="padding:15px">GET Assignments using Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/assignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetblockAttributes1(cidr, callback)</td>
    <td style="padding:15px">Update netblock attributes using Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directAssign1(cidr, body, callback)</td>
    <td style="padding:15px">Direct assign by using netblock cidr and resource id</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/direct_assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirstAvailableIp1(cidr, callback)</td>
    <td style="padding:15px">Get by using Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/first_available?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetblockIpamAttributes1(cidr, callback)</td>
    <td style="padding:15px">Get IPAM Attributes by netblock cidr</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/ipam_attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveNetblockIpamAttributes1(cidr, attribute, limit, offset, q, callback)</td>
    <td style="padding:15px">Get IPAM Attribute by netblock cidr</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/ipam_attributes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mergeNetblock1(cidr, body, callback)</td>
    <td style="padding:15px">Aggregates by using Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/merge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetblockOptions1(cidr, callback)</td>
    <td style="padding:15px">Get netblock options using Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetblockRules1(cidr, callback)</td>
    <td style="padding:15px">Get netblock rules using Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scanNetblock1(cidr, callback)</td>
    <td style="padding:15px">Scan Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/scan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetblockScanResults(cidr, callback)</td>
    <td style="padding:15px">GET Netblock Scan results</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/scan_results?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">splitNetblock1(cidr, body, callback)</td>
    <td style="padding:15px">Splits netblock by using Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/split?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignNetblock1(cidr, body, callback)</td>
    <td style="padding:15px">Unassign netblock using Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/{pathv2}/unassign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetblockUtilization1(cidr, mask, callback)</td>
    <td style="padding:15px">Get netblock utilization using Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetblockVlan1(cidr, callback)</td>
    <td style="padding:15px">Get by using Netblock CIDR</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/vlan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetblock(id, force, callback)</td>
    <td style="padding:15px">Deletes an aggregate rules using Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveNetblock(id, callback)</td>
    <td style="padding:15px">Retrieve Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchNetblock(id, body, callback)</td>
    <td style="padding:15px">Update netblock by using Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putNetblock(id, body, callback)</td>
    <td style="padding:15px">Update netblock by using Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetblockAssignments(id, offset, limit, callback)</td>
    <td style="padding:15px">GET Assignments using Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/assignments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetblockAttributes(id, callback)</td>
    <td style="padding:15px">Update netblock attributes using Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">directAssign(id, body, callback)</td>
    <td style="padding:15px">Direct assign by using netblock id and resource id</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/direct_assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirstAvailableIp(id, callback)</td>
    <td style="padding:15px">Get by using using Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/first_available?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetblockIpamAttributes(id, callback)</td>
    <td style="padding:15px">Get IPAM Attributes by netblock id</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/ipam_attributes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveNetblockIpamAttributes(id, attribute, limit, offset, q, callback)</td>
    <td style="padding:15px">Retrieve IPAM Attribute by netblock id</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/ipam_attributes/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mergeNetblock(id, body, callback)</td>
    <td style="padding:15px">Aggregates by using Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/merge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetblockMetaValues(metaId, id, limit, offset, callback)</td>
    <td style="padding:15px">GET unique meta values for an aggregate</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/meta_values/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetblockOptions(id, callback)</td>
    <td style="padding:15px">Get netblock options using Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/options?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetblockResourceHierarchy(id, callback)</td>
    <td style="padding:15px">GET Netblock resource hierarchy</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/resource_hierarchy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetblockRules(id, callback)</td>
    <td style="padding:15px">Get netblock rules using Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scanNetblock(id, callback)</td>
    <td style="padding:15px">Scan Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/scan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">splitNetblock(id, body, callback)</td>
    <td style="padding:15px">Splits netblock by using Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/split?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignNetblock(id, body, callback)</td>
    <td style="padding:15px">Unassign netblock using Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/unassign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetblockUtilization(id, mask, callback)</td>
    <td style="padding:15px">Get netblock utilization using Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetblockVlan(id, callback)</td>
    <td style="padding:15px">Get by using Netblock ID</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/vlan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostUtilization(type, rir, genericCode, regionId, vlan, tags, callback)</td>
    <td style="padding:15px">Host utilization</td>
    <td style="padding:15px">{base_path}/{version}/ipam/netblocks/{pathv1}/host_utilization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRocessHolding(body, callback)</td>
    <td style="padding:15px">Process holding tank</td>
    <td style="padding:15px">{base_path}/{version}/ipam/process_holding?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRegions(limit, offset, q, callback)</td>
    <td style="padding:15px">Get Regions</td>
    <td style="padding:15px">{base_path}/{version}/ipam/regions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postRegion(body, callback)</td>
    <td style="padding:15px">Create Region</td>
    <td style="padding:15px">{base_path}/{version}/ipam/regions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRegion(id, callback)</td>
    <td style="padding:15px">Delete region</td>
    <td style="padding:15px">{base_path}/{version}/ipam/regions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRegion(id, body, callback)</td>
    <td style="padding:15px">Update region</td>
    <td style="padding:15px">{base_path}/{version}/ipam/regions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAggregatesByRegion(id, recentAssignments, callback)</td>
    <td style="padding:15px">Get list of top level aggregates for a region</td>
    <td style="padding:15px">{base_path}/{version}/ipam/regions/{pathv1}/top_aggregates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRirList(callback)</td>
    <td style="padding:15px">GET RIR List</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rir?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamRules(id, q, loadPositions, limit, offset, orderBy, sort, callback)</td>
    <td style="padding:15px">GET IPAM Rules</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createIpamRule(body, callback)</td>
    <td style="padding:15px">Create IPAM Rule</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamRule(id, callback)</td>
    <td style="padding:15px">Delete IPAM Rule</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveIpamRule(id, callback)</td>
    <td style="padding:15px">Retrieve IPAM Rule</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchIpamRule(id, body, callback)</td>
    <td style="padding:15px">Update IPAM Rule (PATCH)</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIpamRule(id, body, callback)</td>
    <td style="padding:15px">Update IPAM Rule (PUT)</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">previewIpamRule(id, body, callback)</td>
    <td style="padding:15px">Preview IPAM Rule</td>
    <td style="padding:15px">{base_path}/{version}/ipam/rules/{pathv1}/preview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIpamTags(limit, offset, q, callback)</td>
    <td style="padding:15px">GET IPAM Tags</td>
    <td style="padding:15px">{base_path}/{version}/ipam/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamTag(body, callback)</td>
    <td style="padding:15px">Create IPAM Tag</td>
    <td style="padding:15px">{base_path}/{version}/ipam/tags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIpamTag(tag, callback)</td>
    <td style="padding:15px">Delete IPAM tag</td>
    <td style="padding:15px">{base_path}/{version}/ipam/tags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlans(domainId, vlan, name, tags, offset, limit, unavailable, q, callback)</td>
    <td style="padding:15px">GET Vlans</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVlanDomains(q, attrCustom, tags, offset, limit, callback)</td>
    <td style="padding:15px">Get VLAN Domains</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveDomainVlan(id, callback)</td>
    <td style="padding:15px">Retrieve VLAN</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/domains/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrieveVlan(id, callback)</td>
    <td style="padding:15px">Retrieve VLAN</td>
    <td style="padding:15px">{base_path}/{version}/ipam/vlans/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSizeSizes(size, callback)</td>
    <td style="padding:15px">GET IPAM Sizes</td>
    <td style="padding:15px">{base_path}/{version}/ipam/{pathv1}/sizes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postIpamSwip(netblockId, body, callback)</td>
    <td style="padding:15px">Create IPAM Swip</td>
    <td style="padding:15px">{base_path}/{version}/ipam/swip/{pathv1}/simple?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflows(q, offset, limit, orderBy, sort, callback)</td>
    <td style="padding:15px">Get workflows</td>
    <td style="padding:15px">{base_path}/{version}/workflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWorkflows(body, callback)</td>
    <td style="padding:15px">Create new workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowsconnectors(callback)</td>
    <td style="padding:15px">Get all available ACP Connectors</td>
    <td style="padding:15px">{base_path}/{version}/workflows/connectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowsconnectorsgeneric(callback)</td>
    <td style="padding:15px">Get all available ACP Generic Connectors</td>
    <td style="padding:15px">{base_path}/{version}/workflows/connectors/generic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWorkflowsconnectorsgeneric(body, callback)</td>
    <td style="padding:15px">Create ACP Generic connector</td>
    <td style="padding:15px">{base_path}/{version}/workflows/connectors/generic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowsconnectorsgenericauthMechanisms(callback)</td>
    <td style="padding:15px">Get all available auth mechanisms for an ACP Generic Connector</td>
    <td style="padding:15px">{base_path}/{version}/workflows/connectors/generic/auth_mechanisms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowsconnectorsgenericprotocols(callback)</td>
    <td style="padding:15px">Get all available connection protocols for an ACP Generic Connector</td>
    <td style="padding:15px">{base_path}/{version}/workflows/connectors/generic/protocols?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowsconnectorsgenericid(id, callback)</td>
    <td style="padding:15px">Retrieve information about generic connector</td>
    <td style="padding:15px">{base_path}/{version}/workflows/connectors/generic/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWorkflowsconnectorsgenericid(id, body, callback)</td>
    <td style="padding:15px">Update ACP Generic connector</td>
    <td style="padding:15px">{base_path}/{version}/workflows/connectors/generic/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWorkflowsconnectorsconnectorcallsquery(connector, body, callback)</td>
    <td style="padding:15px">Query for connector calls</td>
    <td style="padding:15px">{base_path}/{version}/workflows/connectors/{pathv1}/calls/query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowsconnectorsconnectorcallsfamily(connector, family, callback)</td>
    <td style="padding:15px">Get connector's calls</td>
    <td style="padding:15px">{base_path}/{version}/workflows/connectors/{pathv1}/calls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWorkflowsconnectorsconnectortestCredentials(connector, body, callback)</td>
    <td style="padding:15px">Test connector's credentials</td>
    <td style="padding:15px">{base_path}/{version}/workflows/connectors/{pathv1}/test_credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWorkflowsvalidate(body, callback)</td>
    <td style="padding:15px">Validate workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflows/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWorkflowsid(id, callback)</td>
    <td style="padding:15px">Delete an existing workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowsid(id, callback)</td>
    <td style="padding:15px">Retrieve workflow data</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWorkflowsidclone(id, body, callback)</td>
    <td style="padding:15px">Clone workflow</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkflowsiddownload(id, callback)</td>
    <td style="padding:15px">Download workflow data</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWorkflowsidexecute(id, callback)</td>
    <td style="padding:15px">Execute by using workflow ID</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/execute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWorkflowsidrollbackcallId(id, callId, callback)</td>
    <td style="padding:15px">Rollback using workflow ID</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/rollback/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWorkflowsidtestcallId(id, callId, callback)</td>
    <td style="padding:15px">Test by using workflow ID</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/test/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWorkflowsslugexecute(slug, callback)</td>
    <td style="padding:15px">Execute by using workflow slug</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/execute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWorkflowsslugrollbackcallId(slug, callId, callback)</td>
    <td style="padding:15px">Rollback using workflow slug</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/rollback/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWorkflowsslugtestcallId(slug, callId, callback)</td>
    <td style="padding:15px">Test by using workflow slug</td>
    <td style="padding:15px">{base_path}/{version}/workflows/{pathv1}/test/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsergroups(callback)</td>
    <td style="padding:15px">Get usergroups</td>
    <td style="padding:15px">{base_path}/{version}/usergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsergroups(body, callback)</td>
    <td style="padding:15px">Create new usergroup</td>
    <td style="padding:15px">{base_path}/{version}/usergroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsergroupssearch(body, callback)</td>
    <td style="padding:15px">Search for a usergroup</td>
    <td style="padding:15px">{base_path}/{version}/usergroups/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsergroupsid(id, callback)</td>
    <td style="padding:15px">Retrieve usergroup</td>
    <td style="padding:15px">{base_path}/{version}/usergroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsergroupsidpermissionActions(id, callback)</td>
    <td style="padding:15px">Retrieve usergroup permission actions</td>
    <td style="padding:15px">{base_path}/{version}/usergroups/{pathv1}/permission_actions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedulertasks(date, callback)</td>
    <td style="padding:15px">Get all tasks</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedulertasksadvanced(date, callback)</td>
    <td style="padding:15px">Get all available tasks</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/tasks/advanced?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedulertasksavailable(callback)</td>
    <td style="padding:15px">Get all available tasks</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/tasks/available?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSchedulertasksid(id, callback)</td>
    <td style="padding:15px">Delete existing task</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchedulertasksidhistory(id, callback)</td>
    <td style="padding:15px">Retrieve task history</td>
    <td style="padding:15px">{base_path}/{version}/scheduler/tasks/{pathv1}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResources(id, type, slug, name, sort, orderBy, offset, limit, page, q, parentId, loadPermissions, loadAttributes, query, permissionsTrue, permissionsFalse, callback)</td>
    <td style="padding:15px">Get resources</td>
    <td style="padding:15px">{base_path}/{version}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResources(body, callback)</td>
    <td style="padding:15px">Create resource</td>
    <td style="padding:15px">{base_path}/{version}/resources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResourcesquery(body, callback)</td>
    <td style="padding:15px">Resource query</td>
    <td style="padding:15px">{base_path}/{version}/resources/query?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourcestopOfTree(sectionId, limit, section, body, callback)</td>
    <td style="padding:15px">Get using section ID</td>
    <td style="padding:15px">{base_path}/{version}/resources/top_of_tree?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResourcesid(id, recursive, callback)</td>
    <td style="padding:15px">Delete resource</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourcesid(id, callback)</td>
    <td style="padding:15px">Retrieve resource</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchResourcesid(id, body, callback)</td>
    <td style="padding:15px">Update resource (patch)</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putResourcesid(id, body, callback)</td>
    <td style="padding:15px">Update resource (PUT)</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourcesidattachments(id, attachmentId, callback)</td>
    <td style="padding:15px">Get resource attachments</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResourcesidattachments(id, body, callback)</td>
    <td style="padding:15px">Attach file to a resource</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/attachments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResourcesidattachmentsattachmentId(id, attachmentId, callback)</td>
    <td style="padding:15px">Delete resource attachment</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/attachments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourcesidattachmentsattachmentId(id, attachmentId, callback)</td>
    <td style="padding:15px">Preview resource attachment</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/attachments/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourcesidattachmentsattachmentIddownload(id, attachmentId, callback)</td>
    <td style="padding:15px">Download resource attachment</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/attachments/{pathv2}/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourcesidattrs(id, callback)</td>
    <td style="padding:15px">Get resource attributes</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/attrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchResourcesidattrs(id, body, callback)</td>
    <td style="padding:15px">Update resource attributes (patch)</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/attrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResourcesidattrs(id, body, callback)</td>
    <td style="padding:15px">Add resource attribute</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/attrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putResourcesidattrs(id, body, callback)</td>
    <td style="padding:15px">Update resource attributes (PUT)</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/attrs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResourcesidattrsattributes(id, attributes, callback)</td>
    <td style="padding:15px">Delete resource attribute</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/attrs/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourcesidbackup(id, callback)</td>
    <td style="padding:15px">Get resource backups</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResourcesidbackup(id, body, callback)</td>
    <td style="padding:15px">Backup a resource</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourcesidchildren(id, type, slug, name, sort, orderBy, offset, limit, page, q, origId, parentId, loadPermissions, loadAttributes, query, permissionsTrue, permissionsFalse, callback)</td>
    <td style="padding:15px">Get resource children</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/children?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResourcesidclone(id, body, callback)</td>
    <td style="padding:15px">Clone resource</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourcesidlinks(id, loadResourceData, orderBy, sort, limit, offset, q, callback)</td>
    <td style="padding:15px">Get resource links</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchResourcesidlinks(id, body, callback)</td>
    <td style="padding:15px">Update resource links</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResourcesidlinks(id, body, callback)</td>
    <td style="padding:15px">Create resource link</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putResourcesidlinks(id, body, callback)</td>
    <td style="padding:15px">Replace resource links</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/links?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourcesidlinksrelations(id, loadResourceData, relations, orderBy, sort, limit, offset, q, callback)</td>
    <td style="padding:15px">Get resource links using relation</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/links/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchResourcesidlinksrelation(id, relation, body, callback)</td>
    <td style="padding:15px">Update resource links for a given relation</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/links/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putResourcesidlinksrelation(id, relation, body, callback)</td>
    <td style="padding:15px">Replace resource links for a given relation</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/links/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteResourcesidlinksrelationlinkId(id, relation, linkId, callback)</td>
    <td style="padding:15px">Delete specific link</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/links/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putResourcesidlinksrelationlinkId(id, relation, linkId, body, callback)</td>
    <td style="padding:15px">Update resource link</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/links/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postResourcesidpush(id, body, callback)</td>
    <td style="padding:15px">Push resource</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/push?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourcesidpushStatuspid(id, pid, callback)</td>
    <td style="padding:15px">Get push resource status</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/push_status/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getResourcesidtopOfTree(id, limit, callback)</td>
    <td style="padding:15px">Resource top of tree</td>
    <td style="padding:15px">{base_path}/{version}/resources/{pathv1}/top_of_tree?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsacls(sort, orderBy, offset, parentId, limit, page, callback)</td>
    <td style="padding:15px">GET ACLs</td>
    <td style="padding:15px">{base_path}/{version}/dns/acls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnsacls(body, callback)</td>
    <td style="padding:15px">Create DNS ACL</td>
    <td style="padding:15px">{base_path}/{version}/dns/acls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnsaclsid(id, recursive, callback)</td>
    <td style="padding:15px">Delete ACL</td>
    <td style="padding:15px">{base_path}/{version}/dns/acls/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsgroups(sort, orderBy, offset, parentId, limit, page, callback)</td>
    <td style="padding:15px">GET DNS Groups</td>
    <td style="padding:15px">{base_path}/{version}/dns/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnsgroups(body, callback)</td>
    <td style="padding:15px">Create DNS Group</td>
    <td style="padding:15px">{base_path}/{version}/dns/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnsgroupsgroupIdaclsid(id, relations, resourceId, groupId, callback)</td>
    <td style="padding:15px">Delete resource link</td>
    <td style="padding:15px">{base_path}/{version}/dns/groups/{pathv1}/acls/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsgroupsgroupIdservers(groupId, orderBy, sort, limit, offset, callback)</td>
    <td style="padding:15px">Get Servers to group</td>
    <td style="padding:15px">{base_path}/{version}/dns/groups/{pathv1}/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDnsgroupsgroupIdservers(groupId, body, callback)</td>
    <td style="padding:15px">Update group servers (PATCH)</td>
    <td style="padding:15px">{base_path}/{version}/dns/groups/{pathv1}/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnsgroupsgroupIdservers(groupId, body, callback)</td>
    <td style="padding:15px">Add server to group</td>
    <td style="padding:15px">{base_path}/{version}/dns/groups/{pathv1}/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnsgroupsgroupIdservers(groupId, body, callback)</td>
    <td style="padding:15px">Update group servers (PUT)</td>
    <td style="padding:15px">{base_path}/{version}/dns/groups/{pathv1}/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnsgroupsgroupIdserversid(id, relations, resourceId, groupId, callback)</td>
    <td style="padding:15px">Delete server from group</td>
    <td style="padding:15px">{base_path}/{version}/dns/groups/{pathv1}/servers/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsgroupsgroupIdzones(groupId, orderBy, sort, limit, offset, callback)</td>
    <td style="padding:15px">Get DNS zones for group</td>
    <td style="padding:15px">{base_path}/{version}/dns/groups/{pathv1}/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnsgroupsid(id, recursive, callback)</td>
    <td style="padding:15px">Delete DNS Group</td>
    <td style="padding:15px">{base_path}/{version}/dns/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsgroupsid(id, callback)</td>
    <td style="padding:15px">Retrieve DNS Group</td>
    <td style="padding:15px">{base_path}/{version}/dns/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDnsgroupsid(id, body, callback)</td>
    <td style="padding:15px">Update DNS Group (PATCH)</td>
    <td style="padding:15px">{base_path}/{version}/dns/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnsgroupsid(id, body, callback)</td>
    <td style="padding:15px">Update DNS Groups (PUT)</td>
    <td style="padding:15px">{base_path}/{version}/dns/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsgroupsidavailableCommands(id, callback)</td>
    <td style="padding:15px">DNS Group available commands</td>
    <td style="padding:15px">{base_path}/{version}/dns/groups/{pathv1}/available_commands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnsgroupsidexecutecommand(id, command, body, callback)</td>
    <td style="padding:15px">Execute GROUP command</td>
    <td style="padding:15px">{base_path}/{version}/dns/groups/{pathv1}/execute/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsservers(sort, orderBy, offset, limit, page, serverBackend, serverType, server, username, password, enableViews, port, snmpType, parentId, callback)</td>
    <td style="padding:15px">GET DNS Servers</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnsservers(body, callback)</td>
    <td style="padding:15px">Create DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnsserversid(id, recursive, callback)</td>
    <td style="padding:15px">Delete DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsserversid(id, serverBackend, serverType, server, username, password, enableViews, port, snmpType, parentId, callback)</td>
    <td style="padding:15px">Retrieve DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDnsserversid(id, body, callback)</td>
    <td style="padding:15px">Update DNS Server (PATCH)</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnsserversid(id, body, callback)</td>
    <td style="padding:15px">Update DNS Server (PUT)</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsserversidavailableCommands(id, callback)</td>
    <td style="padding:15px">DNS Server Available commands</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}/available_commands?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsserversidconfig(id, callback)</td>
    <td style="padding:15px">Get DNS Server configuration</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDnsserversidconfig(id, body, callback)</td>
    <td style="padding:15px">Update DNS Server configuration</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnsserversidconfig(id, body, callback)</td>
    <td style="padding:15px">Create DNS Server configuration</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnsserversidexecutecommand(id, command, body, callback)</td>
    <td style="padding:15px">Execute DNS Server command</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}/execute/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsserversidmonitor(id, callback)</td>
    <td style="padding:15px">Monitor DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}/monitor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnsserversidpush(id, body, callback)</td>
    <td style="padding:15px">Push DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}/push?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnsserversidpushConfiguration(id, body, callback)</td>
    <td style="padding:15px">Push DNS Server configuration</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}/push_configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsserversidpushStatus(id, pid, callback)</td>
    <td style="padding:15px">DNS Server push status</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}/push_status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsserversidpushedConfiguration(id, callback)</td>
    <td style="padding:15px">Current DNS Server Pushed configuration</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}/pushed_configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsserversserverIdzones(serverId, orderBy, sort, limit, offset, zoneType, relations, callback)</td>
    <td style="padding:15px">Get attached zones to DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnsserversserverIdzones(serverId, body, callback)</td>
    <td style="padding:15px">Add zone to DNS Server</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnsserversserverIdzonesid(serverId, id, callback)</td>
    <td style="padding:15px">Delete zone from DNS server</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}/zones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnsserversserverIdzonesrelations(serverId, relations, orderBy, sort, limit, offset, zoneType, callback)</td>
    <td style="padding:15px">Get attached zones to DNS Server with specified relation</td>
    <td style="padding:15px">{base_path}/{version}/dns/servers/{pathv1}/zones/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnszones(sort, orderBy, name, zoneType, offset, parentId, limit, page, callback)</td>
    <td style="padding:15px">Get DNS Zones</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnszonesrecordsvalidate(body, callback)</td>
    <td style="padding:15px">Validate DNS Record</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/records/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnszonesid(id, recursive, callback)</td>
    <td style="padding:15px">Delete dns zone</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDnszonesid(id, body, callback)</td>
    <td style="padding:15px">Update DNS Zone (PATCH)</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnszonesid(id, body, callback)</td>
    <td style="padding:15px">Update DNS Zone (PUT)</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnszonesidmonitor(id, callback)</td>
    <td style="padding:15px">Monitor DNS Zone</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/monitor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnszonesidpush(id, body, callback)</td>
    <td style="padding:15px">Push DNS Zone</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/push?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnszonesidpushQueue(id, body, callback)</td>
    <td style="padding:15px">Push DNS Zone records in queue</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/push_queue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnszonesidpushStatuspid(id, pid, callback)</td>
    <td style="padding:15px">DNS Zone push status</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/push_status/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnszonesname(name, sort, orderBy, offset, limit, page, callback)</td>
    <td style="padding:15px">Search DNS Zone name</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnszoneszoneIdrecords(zoneId, sort, orderBy, offset, parentId, limit, page, recordType, recordHost, recordValue, q, callback)</td>
    <td style="padding:15px">GET DNS Records</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/records?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnszoneszoneIdrecords(zoneId, body, callback)</td>
    <td style="padding:15px">Create DNS Record</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/records?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnszoneszoneIdrecordsid(id, recursive, zoneId, callback)</td>
    <td style="padding:15px">Delete DNS Record</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/records/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnszoneszoneIdrecordsid(id, recordType, recordHost, recordValue, q, zoneId, callback)</td>
    <td style="padding:15px">Retrieve DNS Record</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/records/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchDnszoneszoneIdrecordsid(zoneId, id, body, callback)</td>
    <td style="padding:15px">Update DNS Record (PATCH)</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/records/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDnszoneszoneIdrecordsid(zoneId, id, body, callback)</td>
    <td style="padding:15px">Update dns record (PUT)</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/records/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnszoneszoneIdrecordsidmonitor(zoneId, id, callback)</td>
    <td style="padding:15px">Monitor DNS Record</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/records/{pathv2}/monitor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnszoneszoneIdrecordsrecordHost(zoneId, recordHost, sort, orderBy, offset, limit, page, recordType, recordValue, q, callback)</td>
    <td style="padding:15px">Search DNS Record by hostname</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/records/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnszoneszoneIdrecordsrecordType(zoneId, recordType, sort, orderBy, offset, limit, page, recordHost, recordValue, q, callback)</td>
    <td style="padding:15px">Search DNS Record by Record type</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/records/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnszoneszoneIdrecordsQueue(zoneId, sort, orderBy, offset, parentId, limit, page, recordType, recordHost, recordValue, q, callback)</td>
    <td style="padding:15px">GET DNS Records in queue</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/records_queue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDnszoneszoneIdrecordsQueueid(id, recursive, zoneId, callback)</td>
    <td style="padding:15px">Delete DNS Record in queue</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/records_queue/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnszoneszoneIdrecordsQueueid(id, recordType, recordHost, recordValue, q, zoneId, callback)</td>
    <td style="padding:15px">Retrieve DNS Record in Queue</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/records_queue/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnszoneszoneType(zoneType, sort, orderBy, name, offset, limit, page, callback)</td>
    <td style="padding:15px">Get DNS Zones by zone type</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDnszoneszoneType(zoneType, body, callback)</td>
    <td style="padding:15px">Create DNS Zone</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDnszoneszoneTypename(id, name, zoneType, body, callback)</td>
    <td style="padding:15px">Retrieve by zone id</td>
    <td style="padding:15px">{base_path}/{version}/dns/zones/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
