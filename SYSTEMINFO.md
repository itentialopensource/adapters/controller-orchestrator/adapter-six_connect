# 6connect

Vendor: 6connect
Homepage: https://www.6connect.com/

Product: ProVision
Product Page: https://www.6connect.com/6connect-provision/

## Introduction
We classify 6connect into the Data Center domain as 6connect provides capabilities to automate provisioning, configuration, and monitoring of resources. We also classify 6connect into the Network Services domain since it provides functionalities for managing and automating network services such as IP address management, DNS, and DHCP.

"ProVision, 6connect’s unique Dynamic Network Provisioning (DNP) platform uses a robust Connector Library to automate the provisioning, configuration, and control of devices across distributed networks present in datacenters around the world"

## Why Integrate
The 6connect adapter from Itential is used to integrate the Itential Automation Platform (IAP) with 6connect ProVision. 

With this adapter you have the ability to perform operations with 6connect such as:

- IPAM
- DNS
- Region
- VLAN

## Additional Product Documentation
The [API documents for 6connect](https://docs.6connect.com/display/DOC/Home)