# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Six_connect System. The API that was used to build the adapter for Six_connect is usually available in the report directory of this adapter. The adapter utilizes the Six_connect API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The 6connect adapter from Itential is used to integrate the Itential Automation Platform (IAP) with 6connect ProVision. 

With this adapter you have the ability to perform operations with 6connect such as:

- IPAM
- DNS
- Region
- VLAN

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
