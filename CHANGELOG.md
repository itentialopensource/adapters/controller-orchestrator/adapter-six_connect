
## 0.9.4 [10-15-2024]

* Changes made at 2024.10.14_21:36PM

See merge request itentialopensource/adapters/adapter-six_connect!23

---

## 0.9.3 [09-17-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-six_connect!21

---

## 0.9.2 [08-15-2024]

* Changes made at 2024.08.14_19:56PM

See merge request itentialopensource/adapters/adapter-six_connect!20

---

## 0.9.1 [08-07-2024]

* Changes made at 2024.08.06_22:04PM

See merge request itentialopensource/adapters/adapter-six_connect!19

---

## 0.9.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!18

---

## 0.8.5 [03-28-2024]

* Changes made at 2024.03.28_13:48PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!17

---

## 0.8.4 [03-14-2024]

* Fix fields schema mapping

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!16

---

## 0.8.3 [03-13-2024]

* Fix field schema mapping

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!15

---

## 0.8.2 [03-11-2024]

* Changes made at 2024.03.11_16:03PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!14

---

## 0.8.1 [02-28-2024]

* Changes made at 2024.02.28_11:33AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!13

---

## 0.8.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!12

---

## 0.7.1 [03-14-2023]

* Add a task to ipam entity

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!11

---

## 0.7.0 [07-25-2022]

* Changes for smartAssign -- added smartAssignTags and smartAssignObject for requested changes

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!10

---

## 0.6.1 [06-04-2022]

* Add schema for query parameter changes in names

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!9

---

## 0.6.0 [05-20-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!8

---

## 0.5.3 [03-15-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!7

---

## 0.5.2 [07-10-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!6

---

## 0.5.1 [01-14-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!5

---

## 0.5.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!4

---

## 0.4.0 [09-16-2019] & 0.3.0 [09-16-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!3

---
## 0.2.0 [07-30-2019]

- Migrate the adapter to the latest adapter foundation, categorize it, prepare it for app artifact and add ipam entity support.

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!2

---

## 0.1.2 [04-23-2019]

- Adds keys to the package.json and fixes the versions of things that were used to build this adapter

See merge request itentialopensource/adapters/adapter-six_connect!1

---

## 0.1.1 [04-23-2019]

- Initial Commit

See commit 720880e

---
