
## 0.7.1 [03-14-2023]

* Add a task to ipam entity

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!11

---

## 0.7.0 [07-25-2022]

* Changes for smartAssign -- added smartAssignTags and smartAssignObject for requested changes

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!10

---

## 0.6.1 [06-04-2022]

* Add schema for query parameter changes in names

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!9

---

## 0.6.0 [05-20-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!8

---

## 0.5.3 [03-15-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!7

---

## 0.5.2 [07-10-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!6

---

## 0.5.1 [01-14-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!5

---

## 0.5.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!4

---

## 0.4.0 [09-16-2019] & 0.3.0 [09-16-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!3

---
## 0.2.0 [07-30-2019]

- Migrate the adapter to the latest adapter foundation, categorize it, prepare it for app artifact and add ipam entity support.

See merge request itentialopensource/adapters/controller-orchestrator/adapter-six_connect!2

---

## 0.1.2 [04-23-2019]

- Adds keys to the package.json and fixes the versions of things that were used to build this adapter

See merge request itentialopensource/adapters/adapter-six_connect!1

---

## 0.1.1 [04-23-2019]

- Initial Commit

See commit 720880e

---
